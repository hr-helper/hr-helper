import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';

Vue.use(Vuex)

const endpoint = 'http://www.hrhelper.na4u.ru/api/';

export default new Vuex.Store({
  state: {
    employees: null,
    cards: null,
    personnel: null,
    status: '',
    token: localStorage.getItem('token') || '',
    user: {
      name: localStorage.getItem('username') || '',
      id: localStorage.getItem('id') || ''
    },
    
  },
  getters: {
    EMPLOYEES: state => {
      return state.employees;
    },
    CARDS_COUNT: state => payload => {
      if(state.employees != null) {
        let cardsCount = state.employees.filter(employee => employee.cardId === payload).length 
      return cardsCount;
      }
    },

    STARRED: state => {
      if(state.employees != null) {
        var starredArray = state.employees.filter(employee => employee.starred)
      return starredArray;
      }
    },
    CARDS: state => {
      return state.cards;
    },
    PERSONNEL: state => {
      return state.personnel;
    },
    AUTHSTATUS: state => state.status,
    IS_LOGGED_IN: state => !!state.token,
    USER: state => state.user
  },
  mutations: {
    ADD_EMPLOYEE: (state, payload) => {
      state.employees.push(payload);
    },
    SET_EMPLOYEES: (state, payload) => {
      state.employees = payload;
    },
    ADD_CARD: (state, payload) => {
      state.cards.push(payload);
    },
    SET_CARDS: (state, payload) => {
      state.cards = payload;
    },
    SET_PERSONNEL: (state, payload) => {
      state.personnel = payload;
    },
    AUTH_REQUEST: (state) => {
      state.status = 'loading'
    },
    AUTH_SUCCESS: (state, payload) => {
      state.status = 'success'
      state.token = payload.token
      state.user = {
        name: payload.username,
        id: payload.id
      }
    },
    AUTH_ERROR: (state) => {
      state.status = 'error'
    },
    LOGOUT: (state) => {
      state.status = ''
      state.token = ''
    }
  },
  actions: {
    GET_EMPLOYEES: async (context) => {
      let {data} = await Axios.get(endpoint + 'employees/');
      context.commit('SET_EMPLOYEES', data);
    },
    /* SAVE_EMPLOYEE: async (context, payload) => {
      let {data} = await Axios.post('https://my-json-server.typicode.com/yellnessity/json-api/employees', payload);
      if (data.status == 200) {
        context.commit('ADD_EMPLOYEE', payload);
      }
    }, */
    SET_STAR: async (context, payload) => {
      let {data} = await Axios.put(endpoint+'empl_detail/'+payload.id+'/', payload);
      if (data.status == 200) {
        console.log(data);
      }
    },
    GET_CARDS: async (context) => {
      let {data} = await Axios.get(endpoint+'cards/');
      context.commit('SET_CARDS', data); 
    },
    GET_PERSONNEL: async (context) => {
      let {data} = await Axios.get(endpoint+'personnel/');
      context.commit('SET_PERSONNEL', data);
    },
    GET_POSITION_CARDS: async (context, payload) => {
      let {data} = await Axios.get(endpoint+'/empl_workerscards?search='+ payload);
      return data;
    },
    GET_USER_DATA: async (context, payload) => {
      let userId = payload
      console.log(userId)
      let {data} = await Axios.get(endpoint+'users_list/'+ userId+"/");
      return data;
    },
    GET_EMPLOYEE_COUNT: async (context, payload) => {
      let positionId = payload.id
      let {data} = await Axios.get(endpoint+'cards/'+ positionId +'/employees');
      return data.length;
    },
    LOGIN(context, payload){
      return new Promise((resolve, reject) => {
        context.commit('AUTH_REQUEST')
        Axios({url: 'http://www.hrhelper.na4u.ru/api/login/', data: payload, method: 'POST' })
        .then(resp => {
          localStorage.setItem('token', resp.data.token)
          localStorage.setItem('username', resp.data.username)
          localStorage.setItem('id', resp.data.id)
          Axios.defaults.headers.common['Authorization'] = resp.data.token
          context.commit('AUTH_SUCCESS', resp.data)
          resolve(resp)
        })
        .catch(err => {
          context.commit('AUTH_ERROR')
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    REGISTER(context, payload) {
      return new Promise((resolve, reject) => {
        context.commit('AUTH_REQUEST')
        Axios({url: 'http://www.hrhelper.na4u.ru/api/register/', data: payload, method: 'POST' })
        .then(resp => {
          const token = resp.data.token
          const user = resp.data.username
          localStorage.setItem('token', token)
          localStorage.setItem('username', user)
          Axios.defaults.headers.common['Authorization'] = token
          context.commit('AUTH_SUCCESS', token, user)
          resolve(resp)
        })
        .catch(err => {
          context.commit('AUTH_ERROR', err)
          localStorage.removeItem('token')
          localStorage.removeItem('username')
          reject(err)
        })
      })
    },
    LOGOUT(context){
      return new Promise((resolve) => {
        context.commit('LOGOUT')
        localStorage.removeItem('token')
        localStorage.removeItem('username')
        localStorage.removeItem('id')
        delete Axios.defaults.headers.common['Authorization']
        resolve()
      })
    }

  }
})
