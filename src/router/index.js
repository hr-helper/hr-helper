import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Enter from '../views/Enter.vue'
import Registration from '../views/Registration.vue'
import Vuelidate from 'vuelidate'
import About from '../views/About.vue'
import Workers from '../views/Workers.vue'
import Starred from '../views/Starred.vue'
import Dashboard from '../views/Dashboard.vue'
import Applicants from '../views/Applicants.vue'
import Settings from '../views/Settings.vue'
import PositionPage from '../views/PositionPage.vue'
import CardsPage from '../views/CardsPage.vue'
import SummaryRating from '../views/SummaryRating.vue'
import Inputmask from 'inputmask'
import store from '../store'


Vue.use(VueRouter)
Vue.use(Vuelidate)
Vue.use(Inputmask)



  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter(to, from, next) {
      if (store.getters.IS_LOGGED_IN) {
        next('/dashboard')
      } else {
        next();
      }
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/enter',
    name: 'Enter',
    component: Enter
  },
  {
    path: '/registration',
    name: 'Registration',
    component: Registration
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/workers',
    name: 'Workers',
    component: Workers,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/starred',
    name: 'Starred',
    component: Starred,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/applicants',
    name: 'Applicants',
    component: Applicants,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/position/:positionPage',
    name: 'PositionPage',
    component: PositionPage,
    props: true,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/position',
    name: 'CardsPage',
    component: CardsPage,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/summary/:summaryPage',
    name: 'SummaryRating',
    component: SummaryRating,
    props: true,
    meta: { 
      requiresAuth: true
    }
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.IS_LOGGED_IN) {
      next()
      return
    }
    next('/') 
  } else {
    next() 
  }
})



export default router
